#!/usr/bin/env python
from timeit import default_timer as clock
from sympy import *


class sin(Basic):

    def __new__(cls, arg):
        if arg == 0:
            return Integer(0)
        else:
            obj = Basic.__new__(cls, (arg,))
            return obj

    def __repr__(self):
        return "sin(%s)" % self.args[0]

    def expand(self):
        print "expand called"
        return self


x = Symbol("x")
print  ((sin(1)+sin(0)+x)**2).expand()
