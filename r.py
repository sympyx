#!/usr/bin/env python
from timeit import default_timer as clock
from sympy import Symbol, Add, Mul, Integer, multinomial_coefficients

N = 20

x = Symbol("x")
y = Symbol("y")
z = Symbol("z")

e = (x+y+z+1)**N

t_tot = clock()
f = (e*(e+1)).expand()
#f = (e).expand()
t_tot = clock()-t_tot

print "done"

print "# of terms:", len(f.args)
print "total time2:", t_tot
